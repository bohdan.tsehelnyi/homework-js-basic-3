// ============THEORY===========================
/*
1. Що таке логічний оператор?

Логічний оператор - це оператор який дозволяє порівнювати змінні та виконувати певні дії на основі результату їх порівняння.

2. Які логічні оператори є в JavaScript і які їх символи?

В JS існує чотири логічні оператори:
- ! (Не);
- || (Або);
- && (І);
- ?? (оператор null-об’єднання);
*/


// ==============PRACTICE=========================



// 1.
// Задачу з підвищенною складністю вирішив зробити з допомогою циклу while, щоб повторювати запитання користувачу.

let age = parseInt(prompt("Введіть будьласка Ваш вік!"));

while(age !== Number(age)){
    age = parseInt(prompt("Введіть будьласка Ваш вік!"));
}
if(age < 12){
    alert("Вибачте! Але ви ще дитина!");
} else if(age >= 12 && age < 18){
    alert("Ви ще підліток!");
}else if(age >= 18){
    alert("Ви вже дорослий!");
}else{
    console.log(NaN);
}
console.log(age);





// 2.

let month = prompt("Введіть будьласка місяць").toLowerCase();

switch(month){

    case "січень":
        console.log(31);
        break;

    case "лютий":
        console.log(28);
        break;   
        
    case "березень":
        console.log(31);
        break;

    case "квітень":
        console.log(30);
        break;

    case "травень":
        console.log(31);
        break;

    case "червень":
        console.log(30);
        break;
        
    case "липень":
        console.log(31);
        break;

    case "серпень":
        console.log(31);
        break;

    case "вересень":
        console.log(30);
        break;

    case "жовтень":
        console.log(31);
        break;

    case "листопад":
        console.log(30);
        break;

    case "грудень":
        console.log(31);
        break;

    default: 
    console.log("Error");
}





